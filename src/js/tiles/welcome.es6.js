var {LiveTile,registerTile} = require("@ombiel/exlib-livetile-tools");
var screenLink = require("-aek/screen-link");
var {getLibraries} = require("@ombiel/exlib-livetile-tools");

var _;

var localStorageKey = "unsw_welcome_tile_display_name";

var libPromise = getLibraries(["lodash"]).then(([lodash])=>{
  _ = lodash;
});

class WelcomeTile extends LiveTile {

  fetchName() {
    var url = screenLink("unsw-livetile-welcome/fetch");
    this.authRequest(url)
    .then((data)=>{
      if(data && data.name) {
        try{
          localStorage[localStorageKey] = data.name;
        }
        catch(e) {} //eslint-disable-line no-empty
        this.displayName = data.name;
        this.render();
      }
    });
  }

  onReady() {
    this.showInitialBlankFace();
    libPromise.then(()=>{
      this.render = _.debounce(this.render,500);
      try {
        this.displayName = localStorage[localStorageKey];
      }
      catch(e) {} // eslint-disable-line no-empty
      this.checkAttributes();
      this.fetchName();
    });
  }

  checkAttributes() {
    var attrs = this.getTileAttributes();
    var background = attrs.image;
    if(background !== this.prevBackground) {
      var image = new Image();
      image.onload = ()=>{
        this.background = background;
        this.render();
      };
      image.src = background;
    }
    // allow displayName to be added for testing
    if(attrs.displayName) {
      this.displayName = attrs.displayName;
      this.render();
    }
  }

  onChange() {
    this.checkAttributes();
  }

  render() {
    if(this.displayName && this.background) {
      if(this.displayName !== this.prevDisplayName || this.background !== this.prevBackground) {
        this.flipFace([
          {
            css:{
              backgroundImage:`url('${this.background}')`,
              backgroundPosition:"center",
              backgroundRepeat:"no-repeat"
            }
          },
          {text:this.displayName,top:"55%",fitText:true,fontSize:40,css:{padding:"10px"}}
        ],{inactive:true});
        this.prevDisplayName = this.displayName;
        this.prevBackground = this.background;
      }
    }
  }

}

registerTile(WelcomeTile,"welcomeTile");
